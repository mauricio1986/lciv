\begin{thebibliography}{}

\bibitem[Amemiya, 1978]{amemiya1978estimation}
Amemiya, T. (1978).
\newblock The estimation of a simultaneous equation generalized probit model.
\newblock {\em Econometrica: Journal of the Econometric Society}, pages
  1193--1205.

\bibitem[Amemiya, 1979]{amemiya1979estimation}
Amemiya, T. (1979).
\newblock The estimation of a simultaneous-equation tobit model.
\newblock {\em International Economic Review}, pages 169--181.

\bibitem[Atella et~al., 2004]{atella2004determinants}
Atella, V., Brindisi, F., Deb, P., and Rosati, F.~C. (2004).
\newblock Determinants of access to physician services in italy: a latent class
  seemingly unrelated probit approach.
\newblock {\em Health economics}, 13(7):657--668.

\bibitem[Chen et~al., 2017]{chen2017consistency}
Chen, J. et~al. (2017).
\newblock Consistency of the mle under mixture models.
\newblock {\em Statistical Science}, 32(1):47--63.

\bibitem[Chen et~al., 2009]{chen2009hypothesis}
Chen, J., Li, P., et~al. (2009).
\newblock Hypothesis test for normal mixture models: The em approach.
\newblock {\em The Annals of Statistics}, 37(5A):2523--2542.

\bibitem[Clark et~al., 2005]{clark2005heterogeneity}
Clark, A., Etil{\'e}, F., Postel-Vinay, F., Senik, C., and Van~der Straeten, K.
  (2005).
\newblock Heterogeneity in reported well-being: evidence from twelve european
  countries.
\newblock {\em The Economic Journal}, 115(502):C118--C132.

\bibitem[Deb and Trivedi, 2002]{deb2002structure}
Deb, P. and Trivedi, P.~K. (2002).
\newblock The structure of demand for health care: latent class versus two-part
  models.
\newblock {\em Journal of health economics}, 21(4):601--625.

\bibitem[Dias, 2006]{dias2006model}
Dias, J.~G. (2006).
\newblock Model selection for the binary latent class model: A monte carlo
  simulation.
\newblock In {\em Data science and classification}, pages 91--99. Springer.

\bibitem[d'Uva and Jones, 2009]{d2009health}
d'Uva, T.~B. and Jones, A.~M. (2009).
\newblock Health care utilisation in europe: new evidence from the echp.
\newblock {\em Journal of health economics}, 28(2):265--279.

\bibitem[Florens et~al., 2008]{florens2008identification}
Florens, J.-P., Heckman, J.~J., Meghir, C., and Vytlacil, E. (2008).
\newblock Identification of treatment effects using control functions in models
  with continuous, endogenous treatment and heterogeneous effects.
\newblock {\em Econometrica}, 76(5):1191--1206.

\bibitem[Formann, 1992]{formann1992linear}
Formann, A.~K. (1992).
\newblock Linear logistic latent class analysis for polytomous data.
\newblock {\em Journal of the American Statistical Association},
  87(418):476--486.

\bibitem[Fox et~al., 2013]{fox2013hypothesis}
Fox, J., Friendly, M., and Weisberg, S. (2013).
\newblock Hypothesis tests for multivariate linear models using the car
  package.
\newblock {\em The R Journal}, 5(1):39--52.

\bibitem[Gopimatj, 1997]{gopimatj1997modeling}
Gopimatj, D.~A. (1997).
\newblock Modeling heterogeneity in discrete choice processes: Application to
  travel demand.
\newblock {\em Transportation Research Part A}, 1(31):86.

\bibitem[Greene, 2004]{greene2004convenient}
Greene, W. (2004).
\newblock Convenient estimators for the panel probit model: Further results.
\newblock {\em Empirical Economics}, 29(1):21--47.

\bibitem[Greene, 2003]{greene2003econometric}
Greene, W.~H. (2003).
\newblock {\em Econometric analysis}.
\newblock Pearson Education India.

\bibitem[Greene and Hensher, 2003]{greene2003latent}
Greene, W.~H. and Hensher, D.~A. (2003).
\newblock A latent class model for discrete choice analysis: contrasts with
  mixed logit.
\newblock {\em Transportation Research Part B: Methodological}, 37(8):681--698.

\bibitem[Greene and Hensher, 2010]{greene2010modeling}
Greene, W.~H. and Hensher, D.~A. (2010).
\newblock {\em Modeling ordered choices: A primer}.
\newblock Cambridge University Press.

\bibitem[Heckman, 1978]{heckman1978dummy}
Heckman, J.~J. (1978).
\newblock Dummy endogenous variables in a simultaneous equation system.
\newblock {\em Econometrica}, 46(4):931--959.

\bibitem[Heckman et~al., 2006]{heckman2006understanding}
Heckman, J.~J., Urzua, S., and Vytlacil, E. (2006).
\newblock Understanding instrumental variables in models with essential
  heterogeneity.
\newblock {\em The Review of Economics and Statistics}, 88(3):389--432.

\bibitem[Henningsen and Toomet, 2011]{henningsen2011maxlik}
Henningsen, A. and Toomet, O. (2011).
\newblock maxlik: A package for maximum likelihood estimation in r.
\newblock {\em Computational Statistics}, 26(3):443--458.

\bibitem[Hess, 2014]{hess2014latent}
Hess, S. (2014).
\newblock Latent class structures: taste heterogeneity and beyond.
\newblock In {\em Handbook of choice modelling}. Edward Elgar Publishing.

\bibitem[Holm and Pedersen, 2007]{holm2007latent}
Holm, A. and Pedersen, M. (2007).
\newblock Latent class binary regression models: identification and estimation.

\bibitem[Jackson et~al., 2011]{jackson2011multi}
Jackson, C.~H. et~al. (2011).
\newblock Multi-state models for panel data: the msm package for r.
\newblock {\em Journal of statistical software}, 38(8):1--29.

\bibitem[Jedidi et~al., 1993]{jedidi1993maximum}
Jedidi, K., Ramaswamy, V., and DeSarbo, W.~S. (1993).
\newblock A maximum likelihood method for latent class regression involving a
  censored dependent variable.
\newblock {\em Psychometrika}, 58(3):375--394.

\bibitem[Maddala, 2002]{maddala2002limited}
Maddala, G. (2002).
\newblock Limited-dependent and qualitative variables in econometrics.

\bibitem[McLachlan and Peel, 2004]{mclachlan2004finite}
McLachlan, G.~J. and Peel, D. (2004).
\newblock {\em Finite mixture models}.
\newblock John Wiley \& Sons.

\bibitem[Moffitt, 2008]{moffitt2008estimating}
Moffitt, R. (2008).
\newblock Estimating marginal treatment effects in heterogeneous populations.
\newblock {\em Annales d'Economie et de Statistique}, pages 239--261.

\bibitem[Nagin and Land, 1993]{nagin1993age}
Nagin, D.~S. and Land, K.~C. (1993).
\newblock Age, criminal careers, and population heterogeneity: Specification
  and estimation of a nonparametric, mixed poisson model.
\newblock {\em Criminology}, 31(3):327--362.

\bibitem[Newey and McFadden, 1994]{newey1994large}
Newey, W.~K. and McFadden, D. (1994).
\newblock Large sample estimation and hypothesis testing.
\newblock {\em Handbook of econometrics}, 4:2111--2245.

\bibitem[Palomino and Sarrias, 2019]{palomino2019monetary}
Palomino, J. and Sarrias, M. (2019).
\newblock The monetary subjective health evaluation for commuting long
  distances in chile: A latent class analysis.
\newblock {\em Papers in Regional Science}.

\bibitem[Revelt and Train, 2000]{revelt2000customer}
Revelt, D. and Train, K. (2000).
\newblock Customer-specific taste parameters and mixed logit: Households'
  choice of electricity supplier.

\bibitem[Ripley et~al., 2013]{ripley2013package}
Ripley, B., Venables, B., Bates, D.~M., Hornik, K., Gebhardt, A., Firth, D.,
  and Ripley, M.~B. (2013).
\newblock Package `mass'.
\newblock {\em Cran R}, 538.

\bibitem[Rivers and Vuong, 1988]{rivers1988limited}
Rivers, D. and Vuong, Q.~H. (1988).
\newblock Limited information estimators and exogeneity tests for simultaneous
  probit models.
\newblock {\em Journal of econometrics}, 39(3):347--366.

\bibitem[Sarrias and Daziano, 2018]{sarrias2018individual}
Sarrias, M. and Daziano, R.~A. (2018).
\newblock Individual-specific point and interval conditional estimates of
  latent class logit parameters.
\newblock {\em Journal of choice modelling}, 27:50--61.

\bibitem[Shen, 2009]{shen2009latent}
Shen, J. (2009).
\newblock Latent class model or mixed logit model? a comparison by transport
  mode choice data.
\newblock {\em Applied Economics}, 41(22):2915--2924.

\bibitem[Skeels and Taylor, 2015]{skeels2015prediction}
Skeels, C.~L. and Taylor, L.~W. (2015).
\newblock Prediction in linear index models with endogenous regressors.
\newblock {\em The Stata Journal}, 15(3):627--644.

\bibitem[Smith and Blundell, 1986]{smith1986exogeneity}
Smith, R.~J. and Blundell, R.~W. (1986).
\newblock An exogeneity test for a simultaneous equation tobit model with an
  application to labor supply.
\newblock {\em Econometrica: Journal of the Econometric Society}, pages
  679--685.

\bibitem[Titterington, 1990]{titterington1990some}
Titterington, D. (1990).
\newblock Some recent research in the analysis of mixture distributions.
\newblock {\em Statistics}, 21(4):619--641.

\bibitem[Vermunt and Van~Dijk, 2001]{vermunt2001nonparametric}
Vermunt, J.~K. and Van~Dijk, L. (2001).
\newblock A nonparametric random-coefficients approach: The latent class
  regression model.
\newblock {\em Multilevel Modelling Newsletter}, 13(2):6--13.

\bibitem[Wang et~al., 1998]{wang1998analysis}
Wang, P., Cockburn, l.~M., and Puterman, M.~L. (1998).
\newblock Analysis of patent data—a mixed-poisson-regression-model approach.
\newblock {\em Journal of Business \& Economic Statistics}, 16(1):27--41.

\bibitem[Wedel and DeSarbo, 1994]{wedel1994review}
Wedel, M. and DeSarbo, W.~S. (1994).
\newblock A review of recent developments in latent class regression models.

\bibitem[Wedel and DeSarbo, 1995]{wedel1995mixture}
Wedel, M. and DeSarbo, W.~S. (1995).
\newblock A mixture likelihood approach for generalized linear models.
\newblock {\em Journal of Classification}, 12(1):21--55.

\bibitem[Wooldridge, 2010]{wooldridge2010econometric}
Wooldridge, J.~M. (2010).
\newblock {\em Econometric analysis of cross section and panel data}.
\newblock MIT press.

\end{thebibliography}
