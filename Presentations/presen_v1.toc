\beamer@endinputifotherversion {3.36pt}
\select@language {english}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{What is a latent class model?}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Endogeneity}{9}{0}{1}
\beamer@sectionintoc {2}{The model}{14}{0}{2}
\beamer@subsectionintoc {2}{1}{Modelling approach and assumptions}{14}{0}{2}
\beamer@subsectionintoc {2}{2}{Maximum likelihood estimator}{20}{0}{2}
\beamer@subsectionintoc {2}{3}{Marginal effects}{24}{0}{2}
\beamer@sectionintoc {3}{Results}{27}{0}{3}
\beamer@subsectionintoc {3}{1}{Monte Carlo experiment}{27}{0}{3}
\beamer@subsectionintoc {3}{2}{\texttt {LcIV} function in R}{32}{0}{3}
\beamer@sectionintoc {4}{Conclusion}{40}{0}{4}
